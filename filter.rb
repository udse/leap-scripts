#! /usr/bin/env ruby

require 'csv'
require 'ostruct'
require 'optparse'

options = OpenStruct.new
options.output = Dir.pwd

parser = OptionParser.new do |opts|
  opts.banner = "Usage: sync.rb [options] <caliper file> <sychronized samples>"

  opts.on("-o", "--output [DIRECTORY]", "Output directory (default: current directory") do |file|
    options.output = IO.new(IO.sysopen(file, "w"), "w")
  end

  opts.on("-h", "--help", "Show this message") do
    puts opts
    exit
  end
end

parser.parse!

if 2 != ARGV.count
  puts parser
  exit  
end

csv_options = { 
  :col_sep => ",", 
  :headers => true,
  :header_converters => :symbol, 
  :converters => :numeric 
}


subjects = CSV.enum_for(:foreach, ARGV[0], csv_options).lazy.map do |row|
  OpenStruct.new(:run => row[:run], 
		 :name => row[:subject], 
                 :range => row[:start]..row[:stop],
                 :csv => CSV.open(row[:subject]+row[:run].to_s, mode = "w",
                                  :write_headers => true, 
                                  :headers => ["cpu", "hdd", "ram", "bridge", "tsc"]))
end

begin

  subject = subjects.next

  CSV.foreach(ARGV[1], csv_options) do |row|
    if subject.range.include? row[:tsc]
      subject.csv << row
    elsif row[:tsc] > subject.range.end
      subject.csv.close
      subject = subjects.next
    end
  end
    
rescue StopIteration => ex
  puts ex
end



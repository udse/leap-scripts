require 'csv'

# ruby filter [filtered_samples]

cpu = 0
ram = 0
hdd = 0
bridge = 0
number = 0 

CSV.foreach(ARGV[0], {  :col_sep => ',',
			:headers => true,
			:header_converters => :symbol,
			:converters=> :numeric }
	   ) do |row|
      cpu += row[:cpu]
      ram += row[:ram]
      hdd += row[:hdd]
      bridge += row[:bridge]
      number = number+1
end
puts "         CPU       RAM       HDD     BRIDGE     SAMPLES"
puts "TOTAL #{cpu.round(3)} #{ram.round(3)} #{hdd.round(3)} #{bridge.round(3)}    #{number}"
puts "AVERAGE #{(cpu.round(3)/number).round(3)} #{(ram.round(3)/number).round(3)} #{(hdd.round(3)/number).round(3)} #{(bridge.round(3)/number).round(3)}    #{number}"

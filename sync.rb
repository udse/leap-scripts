#! /usr/bin/env ruby

require 'csv'
require 'ostruct'
require 'optparse'

options = OpenStruct.new
options.output = $stdout

parser = OptionParser.new do |opts|
  opts.banner = "Usage: sync.rb [options] <raw sample file> <sync file>"

  opts.on("-o", "--output [FILE]", "Output filename (default: #{options.output}") do |file|
    options.output = IO.new(IO.sysopen(file, "w"), "w") 
  end

  opts.on("-h", "--help", "Show this message") do
    puts opts
    exit
  end
end

parser.parse!

if 2 != ARGV.count
  puts parser
  exit  
end

timestamps = IO.foreach(ARGV[1])
  .lazy
  .map { |line| $1.to_i if line =~ /tsc=(\d+)/ }
  .each_cons(2)

samples = CSV.enum_for(:foreach,
                       ARGV[0], 
                       { 
                         :col_sep => ",", 
                         :headers => true,
                         :header_converters => :symbol, 
                         :converters => :numeric 
                       })
  .lazy
  .chunk { |row| row[:sync] > 0 }
  .map { |_, rows| rows }
  .drop(1)

CSV(options.output, { 
      :write_headers => true, 
      :headers => ["cpu", "hdd", "ram", "bridge", "tsc"]
    }) do |csv|
  
  timestamps.zip(samples).each do | (start, stop), rows |
    
    delta = (stop - start) / rows.length
    
    rows.each_with_index do |sample, index| 
      tsc = start + (index * delta)
      
      csv << [sample[:cpu], sample[:hdd], sample[:ram], sample[:bridge], tsc]
    end
  end
end
